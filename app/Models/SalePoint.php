<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalePoint extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;

    protected $fillable = [
        'location',
        'name',
        'description',
        'image',
        'rent',
        'address',
        'address_link',
        'phone'
    ];


    use HasFactory,SoftDeletes;
}
