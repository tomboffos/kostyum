<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'payment_status_id',
        'order_status_id',
        'payment_type_id',
        'delivery_type_id',
        'price',
        'quantity',
        'bonus',
        'spent_bonuses',
        'address_id',
        'courier_id',
        'sale_point_id',
        'vendor_id',
        'auth'
    ];

    public function sale_point()
    {
        return $this->belongsTo(SalePoint::class);
    }

    public function courier()
    {
        return $this->belongsTo(User::class, 'courier_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function delivery_type()
    {
        return $this->belongsTo(DeliveryType::class);
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function order_status()
    {
        return $this->belongsTo(OrderStatus::class);
    }


    public function receivedBonuses()
    {
        return $this->hasMany(BonusHistory::class);
    }

    public function refundOrder($crud = false)
    {
        return
            $this->attributes['order_status_id'] != 5 ?
                "<a class='btn btn-sm btn-link' href='/orders/" . $this->attributes['id'] . "' ><i class='fa fa-search'></i>Вернуть заказ</a>"
                : '';
    }

    public function getAddressCommentaryAttribute()
    {
        return $this->address->flat_number;
    }
}
