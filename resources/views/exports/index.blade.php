<table>
    <thead>
    <tr>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Сумма заказа</th>
        <th>Баланс</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{$user->order_sum}}</td>
            <td>{{$user->balance}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
