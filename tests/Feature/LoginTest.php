<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/auth/login',[
            'name' => 'Asyl',
            'phone' => '+7 707 303-99-19',
            'password' => '11072000a'
        ]);

        $response->dump();

        $response->assertStatus(422);
    }
}
