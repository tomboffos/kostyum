<?php

namespace App\Http\Controllers\Api\User\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Interactions\PaymentTypeResource;
use App\Models\PaymentType;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //
    public function index()
    {
        return PaymentTypeResource::collection(PaymentType::where('id','!=',4)->get());
    }
}
