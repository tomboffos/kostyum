<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Discount;
use Illuminate\Http\Request;

class DiscountController extends Controller
{
    //
    const CONDITIONS = [
        'quantity',
        'amount',
        'sum_amount'
    ];

    public function index()
    {
        return Discount::get();
    }

    public function getDiscount(Request $request)
    {
        if (Discount::where('amount', '<=', $request->user()->order_sum - $request->user()->spentBonusSum() + $request->price)->where('condition', self::CONDITIONS[2])->exists())
            return Discount::where('amount', '<=', $request->user()->order_sum - $request->user()->spentBonusSum() + $request->price)
                ->where('condition', self::CONDITIONS[2])
                ->orderBy('amount', 'desc')->first();
        return null;
    }
}
