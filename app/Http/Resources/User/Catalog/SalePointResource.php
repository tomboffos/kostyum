<?php

namespace App\Http\Resources\User\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class SalePointResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => asset($this->image),
            'location' => json_decode($this->location),
            'description' => $this->description,
            'rent' => $this->rent,
            'address' => $this->address,
            'address_link' => $this->address_link,
            'phone' => $this->phone
        ];
    }
}
