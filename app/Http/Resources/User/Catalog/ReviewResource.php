<?php

namespace App\Http\Resources\User\Catalog;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user' => new UserResource($this->user),
            'advantages' => $this->advantages,
            'disadvantages' => $this->disadvantages,
            'comments' => $this->comments,
            'rating' => $this->rating
        ];
    }
}
