<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('payment_types')->insert([
           'id'=>  1,
           'name' => 'Онлайн оплата',
        ]);

        DB::table('payment_types')->insert([
           'id' => 2,
           'name' => 'Оффлайн оплата'
        ]);
    }
}
