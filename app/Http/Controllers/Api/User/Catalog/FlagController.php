<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Flag;
use Illuminate\Http\Request;

class FlagController extends Controller
{
    //
    public function index(Request $request)
    {
        return Flag::get();
    }
}
