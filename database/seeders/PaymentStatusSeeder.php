<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('payment_statuses')->insert([
            'name' => 'Не оплачено',
            'id' => 1
        ]);

        DB::table('payment_statuses')->insert([
           'name' => 'Оплачено',
           'id' => 2
        ]);
    }
}
