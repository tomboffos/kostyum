<?php

use App\Http\Controllers\Admin\Import\UserImportController;
use App\Http\Controllers\Admin\Refund\OrderController;
use App\Http\Controllers\Api\User\Catalog\CategoryController;
use App\Http\Controllers\Api\Vendor\Interactions\UserVendorController;
use App\Http\Controllers\Payment\EpayPaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::resource('/orders', OrderController::class);
Route::resource('/users/import', UserImportController::class);
Route::post('/users', [UserVendorController::class, 'index']);
Route::post('/category', [CategoryController::class, 'main']);
Route::get('/users/{user}', [UserVendorController::class, 'show']);
Route::get('/epay/{order}', [EpayPaymentController::class, 'index']);

Route::post('/epay/result', function () {
})->name('epay.success');

Route::post('/epay/fail/result', function () {
})->name('epay.fail');


Route::post('/epay/order/{order}',[EpayPaymentController::class,'postSuccess']);
Route::get('/export', [UserImportController::class, 'export']);
