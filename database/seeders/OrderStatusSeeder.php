<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
           'id' => 1,
           'name' => 'Новый заказ'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 2,
            'name' => 'Заказ в процессе'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 3,
            'name' => 'Заказ доставляется'
        ]);

        DB::table('order_statuses')->insert([
            'id' => 4,
            'name' => 'Заказ завершен'
        ]);
    }
}
