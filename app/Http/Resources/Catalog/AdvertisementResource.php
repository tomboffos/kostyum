<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class AdvertisementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => str_contains($this->image, 'https://',) ? $this->image : asset($this->image),
            'link' => $this->link,
            'category' => $this->category ? new CategoryResource($this->category) : null,
            'products'=> ProductResource::collection($this->products)
        ];
    }
}
