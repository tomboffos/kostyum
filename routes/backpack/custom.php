<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('product', 'ProductCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('filter', 'FilterCrudController');
    Route::crud('filter-type', 'FilterTypeCrudController');
    Route::crud('flag', 'FlagCrudController');
    Route::crud('ad', 'AdCrudController');
    Route::crud('advertisement', 'AdvertisementCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('address', 'AddressCrudController');
    Route::crud('payment-type', 'PaymentTypeCrudController');
    Route::crud('delivery-type', 'DeliveryTypeCrudController');
    Route::crud('payment-status', 'PaymentStatusCrudController');
    Route::crud('order-status', 'OrderStatusCrudController');
    Route::crud('role', 'RoleCrudController');
    Route::crud('sale-point', 'SalePointCrudController');
    Route::crud('discount', 'DiscountCrudController');
    Route::crud('user-notification', 'UserNotificationCrudController');
    Route::crud('city', 'CityCrudController');
    Route::crud('order-detail', 'OrderDetailCrudController');
    Route::crud('size', 'SizeCrudController');
    Route::crud('review', 'ReviewCrudController');
}); // this should be the absolute last line of this file