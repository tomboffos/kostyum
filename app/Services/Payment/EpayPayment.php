<?php


namespace App\Services\Payment;


use App\Models\Order;
use Illuminate\Support\Facades\Http;

class EpayPayment
{
    private $client_secret = 'X#q*xZmmbW!LFfaw9';

    private $client_id = 'KOSTYUM.KZ';

    private $terminal_id = '9da10824-6c84-4949-bd97-70322464cfd1';

    private $test_url = 'https://testoauth.homebank.kz/epay2/oauth2/token';

    private $prod_url = 'https://epay-oauth.homebank.kz/oauth2/token';

    private $currency = 'KZT';

    public function getPaymentLink(Order $order)
    {

        $response = Http::asForm()->post("$this->prod_url", [
            'grant_type' => "client_credentials",
            'scope' => 'payment',
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'invoiceID' => $order->created_at->day . $order->created_at->month . $order->created_at->year . $order->id,
            'currency' => $this->currency,
            'amount' => $order->spent_bonuses != 0 ? $order->price - $order->spent_bonuses : $order->price,
            'terminal' => $this->terminal_id,
            'postLink' => route('epay.success'),
            'failurePostLink' => route('epay.fail')
        ]);

        $order->update(['auth' => $response->body()]);

        return $response->body();
    }

    public function constructPaymentModel(Order $order)
    {
        return [
            'grant_type' => "client_credentials",
            'scope' => 'payment',
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'invoiceID' => $order->id,
            'currency' => $this->currency,
            'amount' => $order->price,
            'terminal' => $this->terminal_id,
//            'postLink' => route('epay.success'),
//            'failurePostLink' => route('epay.fail')
        ];
    }

}
