<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Filter extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;


    protected $fillable = [
        'category_id', 'name', 'filter_type_id',
        'slug',
        'values',
        'order',
        'is_show'
    ];

    protected $casts = [
    ];

    use HasFactory, SoftDeletes;

    public function values()
    {
        return $this->hasMany(FilterValue::class);
    }

    public function possibleValues($category, $filter)
    {
        $values = [];

        foreach (Product::where('category_id', $category)
                     ->whereNotNull('characteristics')
                     ->pluck('characteristics') as $item) {
            foreach (json_decode($item, true) as $filterEntity)
                if (!in_array($filterEntity['value'], $values) && $filterEntity['filter'] == $filter) $values[] = $filterEntity['value'];
        }

        return $values;
    }

    public function filterType()
    {
        return $this->belongsTo(FilterType::class);
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'filter_category');
    }

    public function filter_type()
    {
        return $this->belongsTo(FilterType::class);
    }
}
