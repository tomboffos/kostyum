<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class EpayPaymentController extends Controller
{
    //
    public function index(Order $order)
    {
        return view('payment.epay', [
            'order' => new OrderResource($order),
            'invoice_id' => $order->created_at->day . $order->created_at->month . $order->created_at->year . $order->id
        ]);

    }

    public function postSuccess(Order $order)
    {
        $order->update([
            'payment_status_id' => 2
        ]);
    }
}
