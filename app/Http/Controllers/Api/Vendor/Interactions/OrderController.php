<?php

namespace App\Http\Controllers\Api\Vendor\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Vendor\Interactions\OrderStoreRequest;
use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $service;

    public function __construct()
    {
        $this->service = new OrderService();
    }

    //
    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->vendorOrders($request));
    }


    public function store(OrderStoreRequest $request)
    {

        $result = $this->service->mergeOrderArraysForVendors($request);
        $order = Order::create($result);
        return new OrderResource($order);
    }

    public function update(Order $order, Request $request)
    {
        $order->update($request->all());

        return new OrderResource($order);
    }
}
