<?php


namespace App\Services\Interactions;


use App\Models\SmsCode;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SmsService
{
    public function checkSms($code, User $user)
    {
        return SmsCode::where('code', $code)->where('user_id', $user->id)->exists();
    }

    public function createOrUpdateSms(User $user)
    {
        $sms = SmsCode::where('user_id', $user->id)->first();
        if ($sms) {
            $sms->update([
                'code' => mt_rand(1111, 9999)
            ]);
            return $sms;
        } else
            return SmsCode::create([
                'user_id' => $user->id,
                'code' => mt_rand(1111, 9999)
            ]);
    }

    public static function send($phone, $message)
    {
        $response = Http::get("https://smsc.kz/sys/send.php?login=kostyumkz&psw=987654321a&phones=$phone&mes=$message");

        Log::info('message '.$response->body());
    }
}
