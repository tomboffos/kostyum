<?php

namespace App\Http\Requests\Api\User\Info;

use Illuminate\Foundation\Http\FormRequest;

class AddressStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'street' => 'required',
            'flat_number' => 'nullable',
            'entrance' => 'nullable',
            'city_id' => 'required|exists:cities,id'
        ];
    }
}
