<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'balance' => $this->balance,
            'role_id' => $this->role_id,
            'image' => $this->image,
            'order_count' => $this->ordersCount(),
            'id' => $this->id,
            'notification' => $this->receive_notifications
        ];
    }
}
