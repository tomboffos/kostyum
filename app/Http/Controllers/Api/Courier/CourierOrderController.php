<?php

namespace App\Http\Controllers\Api\Courier;

use App\Http\Controllers\Controller;
use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class CourierOrderController extends Controller
{
    //
    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->courierOrders);
    }

    public function update(Order $order,Request $request)
    {
        $order->update([
            'order_status_id' => $request->has('order_status_id') ? $request->order_status_id : $order->order_status_id,
            'payment_status_id' => $request->has('payment_status_id') ? $request->payment_status_id : $order->payment_status_id

        ]);

        return new OrderResource($order);
    }
}
