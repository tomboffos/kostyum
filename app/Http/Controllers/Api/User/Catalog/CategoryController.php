<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\Catalog\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    public function index()
    {
        return CategoryResource::collection(Category::whereNull('category_id')->orderBy('order','asc')->get());
    }

    public function show(Category $category)
    {
        return CategoryResource::collection($category->children);
    }

}
