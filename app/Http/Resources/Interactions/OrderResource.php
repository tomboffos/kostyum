<?php

namespace App\Http\Resources\Interactions;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'delivery_type' => $this->delivery_type,
            'payment_type' => $this->payment_type,
            'address' => $this->address,
            'items' => OrderDetailResource::collection($this->items),
            'bonus' => $this->bonus,
            'spent_bonuses' => $this->spent_bonuses,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'created_at' => $this->created_at->format('d.m.y'),
            'order_status' => $this->order_status,
            'payment_status' => $this->payment_status,
            'auth' => $this->auth
        ];
    }
}
