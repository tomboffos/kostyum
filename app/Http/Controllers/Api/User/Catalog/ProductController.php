<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\Catalog\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function index(Request $request)
    {
//        dd($request->filters);
        return ProductResource::collection(Product::where(function ($query) use ($request) {
            if ($request->has('rent'))
                $query->where('rent', 1);
            if ($request->has('main_page'))
                $query->where('main_page', 1);
            if ($request->has('search'))
                $query->where('name', 'LIKE', '%' . $request->search . '%');
            if ($request->has('category'))
                $query->where('category_id', $request->category);
            if ($request->has('prices'))
                $query->whereBetween('price', $request->prices);
            if ($request->has('flags'))
                $query->whereHas('flags', function ($secondQuery) use ($request) {
                    $secondQuery->whereIn('id', $request->flags);
                });
            if ($request->has('filters')) {
                $query->whereJsonContains('characteristics', $request->filters);
            }
            if ($request->has('ad'))
                $query->whereHas('advertisements', function ($secondQuery) use ($request) {
                    $secondQuery->where('id', $request->ad);
                });

        })->orderBy($request->sort_type, $request->sort_value)
            ->paginate(10));
    }
}
