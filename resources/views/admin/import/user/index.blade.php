@extends(backpack_view('blank'))
@section('content')
    <div class="row">
        <div class="col-sm-5 bg-dark p-2">

            <h2>Загрузите свой excel файл для пользователей</h2>
            <input type="file" class="form-control-file" id="import_users_file">
            <div class="mt-2">
                <button class="btn btn-primary" id="import_users_button">Импорт</button>

            </div>
        </div>
        <div class="col-sm-2"></div>

        <div class="col-sm-5 bg-dark p-2">

            <h2>Выгрузить excel пользователей</h2>
            <div class="mt-2">
                <button class="btn btn-primary" onclick="location.href='/export'">Экспорт</button>
            </div>
        </div>

    </div>
    <script src="{{asset('js/import.js')}}"></script>
@endsection
