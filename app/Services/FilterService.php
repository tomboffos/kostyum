<?php


namespace App\Services;


use App\Http\Resources\User\Catalog\FilterResource;
use App\Http\Resources\User\Catalog\FilterWithValueResource;
use App\Models\Discount;
use App\Models\Filter;
use App\Models\Product;

class FilterService
{
    private $rus = array('а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ', ' ');
    private $eng = array('a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', ' ', '_');

    public static function showAsFilters($filterArray)
    {
        $mainFilterArray = [];
        foreach (json_decode($filterArray, true) as $item)
            if ($item['filter'] != null)
                $mainFilterArray[] = [
                    'filter' => new FilterResource(Filter::find($item['filter'])),
                    'value' => $item['value'],
                    'order' => strval(Filter::find($item['filter'])->order)
                ];
        return $mainFilterArray;
    }

    public function showFilterResource($filterArray)
    {
        $mainFilterArray = [];
        foreach ($filterArray as $item)
            if (Filter::find($item)->is_show)
                $mainFilterArray[] = new FilterWithValueResource(Filter::find($item));

        return $mainFilterArray;
    }

    public function getFiltersFromCategory($category)
    {
        $filters = Product::where('category_id', $category)
            ->whereNotNull('characteristics')
            ->pluck('characteristics');

        $filtersMain = [];
        foreach ($filters as $item) {
            foreach (json_decode($item, true) as $filterEntity) {
                if (!in_array($filterEntity['filter'], $filtersMain)) $filtersMain[] = $filterEntity['filter'];
            }
        }

        return $this->showFilterResource($filtersMain);
    }

    public function constructCharacteristics($filters)
    {
        $filters = $this->array_except($filters, ['naimenovanie', 'kod_artikula', 'cena', 'opisanie', 'id_tovara', 'tip_tovarov', 'tip_stroki']);

        $mainFilterArray = [];
        foreach ($filters as $key => $value) {
            if ($value != null) {
                $filter = Filter::where('slug', $key)->first();
                if ($filter != null) {
                    $filter->update([
                        'name' => $this->constructFromSlug($key)
                    ]);
                    $mainFilterArray[] = [
                        'filter' => $filter->id,
                        'value' => $value
                    ];
                } else {
                    $filter = Filter::create([
                        'filter_type_id' => 1,
                        'slug' => $key,
                        'name' => $this->constructFromSlug($key)
                    ]);

                    $mainFilterArray[] = [
                        'filter' => $filter->id,
                        'value' => $value
                    ];
                }
            }
        }
        return $mainFilterArray;
    }

    public function array_except($array, $keys)
    {
        foreach ($keys as $key) {
            unset($array[$key]);
        }
        return $array;
    }

    public function constructFromSlug($slug)
    {
        return str_replace($this->eng, $this->rus, $slug);
    }


}
