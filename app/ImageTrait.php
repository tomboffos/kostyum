<?php


namespace App;


class ImageTrait
{
    public static function decodeImageArray($values)
    {
        $images = [];
        if ($values != null && json_decode($values) != null)
            foreach (json_decode($values) as $image) {
                $images[] = str_contains($image, 'https://') ? $image : asset($image);
            }

        return $images;
    }
}
