<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('Продукт', 'Продукты');
        $this->crud->setCreateView('admin.product.create');
        $this->crud->setEditView('admin.product.edit');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Название');
        CRUD::column('images')->type('browse_multiple')->label('Фото');
        CRUD::column('video')->label('Видео');
        CRUD::column('category_id')->label('Категория');
        CRUD::column('stock');
        CRUD::column('discount');
        CRUD::column('price');
        CRUD::column('bonus');
        CRUD::column('vendor');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        CRUD::field('name')->label('Название');
        CRUD::field('images')->type('browse_multiple')->label('Фото');
        CRUD::field('video')->type('text')->label('Ссылка на видео');
        CRUD::field('category_id')->label('Категория')->attributes([
            'id' => 'category'
        ])->type('select');
        CRUD::field('stock')->label('В наличии');
        CRUD::field('discount')->type('hidden')->label('Скидка в процентах')->value(0);
        CRUD::field('price')->type('number')->label('Цена');
        CRUD::field('bonus')->type('hidden')->label('Бонус')->value(0);
        CRUD::field('vendor')->label('Артикул');
        CRUD::field('flags')->label('Флаги');
        CRUD::field('old_price')->label('Старая цена');
        CRUD::field('characteristics')->label("Характеристики")->type('hidden')->attributes([
            'id' => 'characteristics'
        ]);
        CRUD::field('sizes')->label("Размеры")->type('repeatable')->fields([
            [
                'name' => 'size',
                'label' => 'Размер',
                'type' => 'relationship',
                'multiple' => false,
                'model' => 'App\Models\Size',
                'attribute' => 'name'

            ],
            [
                'name' => 'available',
                'label' => 'Доступно',
                'type' => 'checkbox'
            ],
        ]);
        CRUD::field('rent')->label('Доступен для аренды');
        CRUD::field('main_page')->label('Основная страница');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
