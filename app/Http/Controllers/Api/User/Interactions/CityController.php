<?php

namespace App\Http\Controllers\Api\User\Interactions;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    //
    public function index()
    {
        return City::get();
    }
}
