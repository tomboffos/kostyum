<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Api\User\Info\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings('пользователь', 'пользователи');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Имя');
        CRUD::column('order_sum')->label('Сумма заказа');
        CRUD::column('email')->label('Почта');
        CRUD::column('balance')->label('Бонусы');
        CRUD::column('phone')->label('Телефон');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserRequest::class);

        CRUD::field('name')->label('Имя');
        CRUD::field('email')->label('Почта');
        CRUD::field('password')->label('Пароль');
        CRUD::field('phone')->label('Телефон');
        CRUD::field('balance')->label('Бонусы');
        CRUD::field('order_sum')->label('Сумма заказа');
        CRUD::field('image')->label('Фото')->type('browse');
        CRUD::field('role_id')->label('Роль');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(UpdateUserRequest::class);

        CRUD::field('name')->label('Имя');
        CRUD::field('email')->label('Почта');
        CRUD::field('password')->label('Пароль');
        CRUD::field('phone')->label('Телефон');
        CRUD::field('balance')->label('Бонусы');
        CRUD::field('order_sum')->label('Сумма заказа');
        CRUD::field('image')->label('Фото')->type('browse');
        CRUD::field('role_id')->label('Роль');

    }
}
