<?php


namespace App\Services\Interactions;


use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PushService
{
    public function insertToken(User $user,$token)
    {
        if (!$user->tokenExists($token))
            UserDevice::create([
               'token' =>  $token,
               'user_id' => $user->id
            ]);
    }



    public static function send($device_tokens, string $title, string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=" . env('PUSH_NOTIFICATION'),
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
        Log::error('logging '.$response->body());

        return $response->successful();
    }


}
