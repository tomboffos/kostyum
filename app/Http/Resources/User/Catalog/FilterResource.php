<?php

namespace App\Http\Resources\User\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class FilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'filter_type' => $this->filter_type,
            'slug' => $this->slug,
            'id' => $this->id,
            'values' => json_decode($this->values)
        ];
    }
}
