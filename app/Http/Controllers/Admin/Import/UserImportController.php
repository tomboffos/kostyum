<?php

namespace App\Http\Controllers\Admin\Import;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Import\UserImportRequest;
use App\Imports\ProductsImport;
use App\Imports\UsersImport;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserImportController extends Controller
{
    //
    public function index()
    {
        return view('admin.import.user.index');
    }

    public function store(UserImportRequest $request)
    {
        if ($request->users)
            Excel::import(new UsersImport, $request->file('import_users')->store('imports'));
        else
            (new ProductsImport)->queue($request->file('import_products')->storeAs('imports', 'laravel-imports.' . $request->file('import_products')->getClientOriginalExtension()), null, \Maatwebsite\Excel\Excel::CSV);
        return response(['message' => 'Успешно'], 200);
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
