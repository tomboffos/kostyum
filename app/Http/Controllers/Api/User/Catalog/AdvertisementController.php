<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\Catalog\AdvertisementResource;
use App\Models\Advertisement;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    //
    public function index()
    {
        return AdvertisementResource::collection(Advertisement::orderBy('order','asc')->get());
    }
}
