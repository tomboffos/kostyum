<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    protected $fillable = [
        'name',
        'bonus_type',
        'payment_status_id',
        'description',
        'value',
        'payment_type_id',
        'amount',
        'condition'
    ];

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    use HasFactory,SoftDeletes;
}
