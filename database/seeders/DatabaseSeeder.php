<?php

namespace Database\Seeders;

use App\Models\Advertisement;
use App\Models\Category;
use App\Models\DeliveryType;
use App\Models\Product;
use App\Models\Review;
use Database\Factories\AdvertisementFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//        Category::factory(10)->create();
//        Advertisement::factory(3)->create();
//        Product::factory(100)->create();
//        Review::factory(50)->create();

        $this->call([
            AddRefundStatusToOrderStatusesSeeder::class
//            RoleAdminSeeder::class
//            RoleAdditionSeeder::class
//            RoleSeeder::class
//            OrderStatusSeeder::class
//            PaymentTypeAddSeeder::class
//            FilterTypeSeeder::class
//           DeliveryTypeSeeder::class,
//           PaymentTypeSeeder::class,
//           PaymentStatusSeeder::class
        ]);
    }
}
