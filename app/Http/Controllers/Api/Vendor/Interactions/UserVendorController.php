<?php

namespace App\Http\Controllers\Api\Vendor\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserVendorController extends Controller
{
    //

    public function index(Request $request)
    {
        return UserResource::collection(User::where(function ($query) use ($request) {
            if ($request->has('search'))
                $query->where('name', 'LIKE', '%' . $request->search . '%')->orWhere('phone', 'LIKE', '%' . $request->search . '%');
            if ($request->has('q'))
                $query->where('name', 'LIKE', '%' . $request->q . '%')->orWhere('phone', 'LIKE', '%' . $request->q . '%');

        })->get());
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }
}
