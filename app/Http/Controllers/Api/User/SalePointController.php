<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Catalog\SalePointResource;
use App\Models\SalePoint;
use Illuminate\Http\Request;

class SalePointController extends Controller
{
    //
    public function index()
    {
        return SalePointResource::collection(SalePoint::orderBy('order','asc')->get());
    }
}
