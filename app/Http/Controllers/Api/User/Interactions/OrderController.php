<?php

namespace App\Http\Controllers\Api\User\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Interactions\OrderStoreRequest;
use App\Http\Resources\Interactions\OrderResource;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Services\OrderService;
use App\Services\Payment\EpayPayment;
use App\Services\Payment\ZoodPayment;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //

    public function index(Request $request)
    {
        return OrderResource::collection($request->user()->orders);
    }

    private $service;
    private $zood;
    private $epay;

    public function __construct()
    {
        $this->service = new OrderService();
        $this->zood = new ZoodPayment();
        $this->epay = new EpayPayment();
    }

    public function store(OrderStoreRequest $request)
    {
        $data = $request->validated();

        $basket = $data['basket'];
        $result = $this->service->mergeOrderArrays($request);

        $order = Order::create($result);

        $this->service->iterateBasket($basket, $order);

        switch ($order->payment_type_id) {
            case 1:
                return response([
                    'order' => new OrderResource($order),
                    'payment' => $this->zood->getPaymentLink($order)
                ], 200);
            case 2:
                return response([
                    'order' => new OrderResource($order),
                    'payment' => null
                ], 200);
            case 3:
                return response([
                    'order' => new OrderResource($order),
                    'payment' => $this->epay->getPaymentLink($order),
                ]);
            default:
                return null;
        }

    }

    public function show(Order $order, Request $request)
    {
        $order->update([
            'payment_type_id' => $request->payment_type_id
        ]);

        switch ($request->payment_type_id) {
            case 1:
                return response([
                    'order' => new OrderResource($order),
                    'payment' => $this->zood->getPaymentLink($order)
                ], 200);
            case 3:
                return response([
                    'order' => new OrderResource($order),
                    'payment' => $this->epay->getPaymentLink($order),
                ]);
            default:
                return null;
        }
    }
}
