<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'images',
        'image',
        'video',
        'category_id',
        'stock',
        'discount',
        'price',
        'bonus',
        'vendor',
        'characteristics',
        'main_page',
        'rent',
        'description',
        'origin_product_id',
        'old_price',
        'sizes'
    ];

    protected $casts = [
    ];

//    public function sizes()
//    {
//        return $this->belongsToMany(Size::class,'products_sizes');
//    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function userCheckFavorite($user)
    {
        return $user->favorites->contains($this);
    }

    public function flags()
    {
        return $this->belongsToMany(Flag::class, 'product_flags');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class)->where('moderated', true);
    }

    public function rating()
    {
        return $this->hasMany(Review::class)->where('moderated', true)->count() != 0 ?
            ($this->hasMany(Review::class)->where('moderated', true)->sum('rating')
                / $this->hasMany(Review::class)->where('moderated', true)->count()) : 0;
    }

    public function setCharacteristicsAttribute($value)
    {
        $this->attributes['characteristics'] = json_encode($value);
    }

    public function advertisements()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisement_products');
    }

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = $value != null ? $value : $this->attributes['images'];
    }
}
