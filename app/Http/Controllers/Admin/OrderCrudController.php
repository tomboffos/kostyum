<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Order::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order');
        CRUD::setEntityNameStrings('Заказ', 'Заказы');
        $this->crud->setCreateView('admin.users.index');
        $this->crud->setShowView('admin.order.read');
        $this->crud->setUpdateView('admin.order.edit');

    }


    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addButtonFromModelFunction('line','return','refundOrder','beginning');

        CRUD::column('id')->label('Номер заказа');
        CRUD::column('user_id')->label('Пользователь')->wrapper([
            'href' => function ($crud, $column, $entry, $related_key) {
                return backpack_url('user/'.$related_key.'/show');
            },
        ]);
        CRUD::column('payment_status_id')->label('Статус оплаты');
        CRUD::column('order_status_id')->label('Статус заказа');
        CRUD::column('payment_type_id')->label('Тип оплаты');
        CRUD::column('delivery_type_id')->label('Тип доставки');
        CRUD::column('price')->label('Цена');
        CRUD::column('quantity')->label('Количество');
        CRUD::column('bonus');
        CRUD::column('spent_bonuses');
        CRUD::column('address_id');
        CRUD::column('courier_id');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);

        CRUD::field('user_id')->label('Пользователь')->data_source('/users')->attributes([
            'id' => 'user'
        ]);
        CRUD::field('payment_status_id')->label('Статус оплаты');
        CRUD::field('order_status_id')->label('Статус заказа');
        CRUD::field('payment_type_id')->label('Тип оплаты');
        CRUD::field('delivery_type_id')->label('Тип доставки');
        CRUD::field('price')->label('Цена');
        CRUD::field('quantity')->label('Количество')->type('hidden')->value('0');
        CRUD::field('bonus')->label('Полученные бонусы')->type('hidden')->value('0');
        CRUD::field('spent_bonuses')->label('Потраченные бонусы');
        CRUD::field('address_id')->label('Адресс');
        CRUD::field('courier_id')->label('Курьер')->type('relationship');
        CRUD::field('sale_point_id')->label('Точка продажи');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function setupShowOperation()
    {
        CRUD::column('user_id')->label('Пользователь');
        CRUD::column('payment_status_id')->label('Статус оплаты');
        CRUD::column('order_status_id')->label('Статус заказа');
        CRUD::column('payment_type_id')->label('Тип оплаты');
        CRUD::column('delivery_type_id')->label('Тип доставки');
        CRUD::column('price')->label('Цена');
        CRUD::column('quantity')->label('Количество');
        CRUD::column('spent_bonuses')->label('Потраченные бонусы');
        CRUD::column('address_id')->label('Адрес');
        CRUD::column('courier_id')->label('Курьер');
        CRUD::column('vendor_id')->label('Продавец');
    }
}
