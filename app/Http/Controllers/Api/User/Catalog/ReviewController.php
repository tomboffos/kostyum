<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Catalog\ReviewStoreRequest;
use App\Http\Resources\User\Catalog\ReviewResource;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    public function store(ReviewStoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;

        return new ReviewResource(Review::create($data));
    }

}
