import axios from "axios";
import Swal from "sweetalert2";

let importUserButton = document.getElementById('import_users_button')
let importProductButton = document.getElementById('import_products_button')
importUserButton.addEventListener('click', function (e) {
    e.preventDefault()

    let value = document.getElementById('import_users_file').files[0]
    let formData = new FormData()
    console.log(value)
    formData.append('import_users', value)
    formData.append('users', 1)

    axios.post('/users/import', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then(response => Swal.fire({
        title: 'Успешно',
        text: response.data.message,
        icon: 'success',
        confirmButtonText: 'Ок'
    }))
})

importProductButton.addEventListener('click',function(e){
    e.preventDefault()
    let value = document.getElementById('import_products_file').files[0]
    let formData = new FormData()
    formData.append('import_products',value)
    formData.append('users',0)

    axios.post('/users/import', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then(response => Swal.fire({
        title: 'Успешно',
        text: response.data.message,
        icon: 'success',
        confirmButtonText: 'Ок'
    }))
})
