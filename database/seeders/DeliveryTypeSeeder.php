<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('delivery_types')->insert([
           'id' => 1,
           'name' => 'Доставка'
        ]);
        DB::table('delivery_types')->insert([
           'id' => 2,
           'name' => 'Самовывоз'
        ]);
    }
}
