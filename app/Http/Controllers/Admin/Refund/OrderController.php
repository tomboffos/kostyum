<?php

namespace App\Http\Controllers\Admin\Refund;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function show(Order $order)
    {
        $order->update([
            'order_status_id' => 5
        ]);

        return redirect()->back();
    }
}
