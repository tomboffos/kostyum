<?php


namespace App\Services;


use App\Models\Size;

class SIzeService
{
    public static function showAsSizesAsAvailable($availableSizes): array
    {
        $sizes = [];
        foreach (json_decode($availableSizes,true) as $item)
            if ($item['available']) $sizes[] = [
                'size' => Size::find($item['size']),
            ];
        return $sizes;
    }
}
