<?php

namespace App\Http\Controllers\Api\User\Interactions;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Interactions\DeliveryTypeResource;
use App\Models\DeliveryType;
use Illuminate\Http\Request;

class DeliverTypeController extends Controller
{
    //
    public function index()
    {
        return DeliveryTypeResource::collection(DeliveryType::get());
    }
}
