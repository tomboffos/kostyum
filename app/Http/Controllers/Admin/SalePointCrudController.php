<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SalePointRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SalePointCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SalePointCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SalePoint::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sale-point');
        CRUD::setEntityNameStrings('точка продаж', 'точки продаж');
        $this->crud->setCreateView('admin.sale_point.create');
        $this->crud->setUpdateView('admin.sale_point.edit');


    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label('Название');
        CRUD::column('description')->label('Описание');
        CRUD::column('image')->label('Фото');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SalePointRequest::class);

        CRUD::field('location')->label('Локация')->type('hidden');
        CRUD::field('name')->label('Название');
        CRUD::field('description')->label('Описание')->type('summernote');
        CRUD::field('image')->label('Фото')->type('browse');
        CRUD::field('rent')->label('Аренда смокингов');
        CRUD::field('order')->label('Порядок')->value(0);
        CRUD::field('address')->label('Адрес');
        CRUD::field('address_link')->label('Ссылка на адрес');
        CRUD::field('phone')->label('Телефон');
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
