<?php

namespace App\Http\Controllers\Api\User\Info;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Info\UpdateUserRequest;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index(Request $request)
    {
        return new UserResource($request->user());
    }

    public function store(UpdateUserRequest $request)
    {
        if ($request->password == null || $request->password = '')
            unset($request->validated()['password']);
        $request->user()->update($request->validated());

        return new UserResource($request->user());
    }
}
