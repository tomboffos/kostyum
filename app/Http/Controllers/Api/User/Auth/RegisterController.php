<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ConfirmationRequest;
use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\Interactions\PushService;
use App\Services\Interactions\SmsService;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    private $push;
    /**
     * @var SmsService
     */
    private $sms;

    public function __construct()
    {
        $this->push = new PushService();
        $this->sms = new SmsService();
    }

    public function register(RegisterRequest $request)
    {
        if (substr($request->phone, 0, 2) != '87')
            return response([
                'message' => 'Пожалуйста введите Казахстанский номер'
            ], 403);

        $token = $request->token;
        $data = $request->validated();
        unset($data['token']);
        $user = User::create($data);

        $this->push->insertToken($user, $token);

        return response([
            'user' => new UserResource($user),
            'sms' => $this->sms->createOrUpdateSms($user),
        ], 201);
    }

    public function check(User $user, ConfirmationRequest $request)
    {
        if ($this->sms->checkSms($request->code, $user)) return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(env('APP_NAME'))->plainTextToken,

        ], 200); else return response([
            'message' => 'Не правильный код'
        ], 400);
    }


}
