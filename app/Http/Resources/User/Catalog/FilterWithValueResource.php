<?php

namespace App\Http\Resources\User\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class FilterWithValueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'filter_type' => $this->filter_type,
            'slug' => $this->slug,
            'values' => $this->possibleValues($request->category,$this->id),
        ];
    }
}
