<?php


namespace App\Services\Payment;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class ZoodPayment
{
    const CITY = 'Almaty';
    const COUNTRY_CODE = 'KZ';
    const STATE = 'ALA';
    const ZIPCODE = '0005060';
    const SERVICE_CODE = 'ZPI';
    const CURRENCY = 'KZT';
    private $merchant_key = 'k@stYumKz';
    const SALT = ']Ja5}vj*';

    private function constructBillingModel($order)
    {
        return [
            'address_line1' => $order->address->street,
            'address_line_2' => $order->address->entrance . ' , ' . $order->address->flat_number,
            'city' => self::CITY,
            'country_code' => self::COUNTRY_CODE,
            'name' => $order->user->name,
            'phone_number' => $order->user->phone,
            'state' => self::STATE,
            'zipcode' => self::ZIPCODE
        ];
    }

    private $merchant_auth = 'A.d7H^AF{3X^{DyA';

    private function constructCustomerModel($order)
    {
        return [
            'first_name' => $order->user->name,
            'customer_email' => $order->user->email != null ? $order->user->email : 'tomboffos@gmail.com',
            'customer_phone' => str_replace('8', '7', str_replace([
                ' ',
                '+',
                '-',
                '(',
                ')',
            ], '', $order->user->phone))
        ];
    }

    public function getPaymentLink($order)
    {
        $response = Http::withBasicAuth($this->merchant_key, $this->merchant_auth)
            ->withHeaders([
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ])->post('https://sandbox-api.zoodpay.com/v0/transactions', [
                'shipping' => $this->constructBillingModel($order),
                'customer' => $this->constructCustomerModel($order),
                'items' => $this->constructItemsBasket($order),
                'order' => $this->constructOrderModel($order)
            ]);

        return $response->json();
    }

    private function constructItemsBasket($order)
    {
        $orderMain = [];


        foreach ($order->items as $item) {
            $orderMain[] = [
                'name' => $item->product->name,
                'quantity' => $item->quantity,
                'price' => $item->product->price,
                'categories' => [
                    [$item->product->category->name],
                    [$item->product->category->name],

                ],
//                'discount_amount' => $item->product->discount,
                'currency_code' => self::CURRENCY,
            ];
        }

        return $orderMain;
    }

    private function constructOrderModel($order)
    {
        return [
            'amount' => $order->spent_bonuses != 0 ? $order->price - $order->spent_bonuses : $order->price,
            'currency' => self::CURRENCY,
            'service_code' => self::SERVICE_CODE,
            'market_code' => self::COUNTRY_CODE,
            'merchant_reference_no' => (string)$order->id,
            'signature' => hash('sha512', implode('|', [
                $this->merchant_key,
                (string)$order->id,
                (string)$order->spent_bonuses != 0 ? $order->price - $order->spent_bonuses : $order->price,
                self::CURRENCY,
                self::COUNTRY_CODE,
                self::SALT
            ])),
            'lang' => 'ru'
        ];
    }
}
