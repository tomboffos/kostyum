<?php

namespace App\Http\Resources\Catalog;

use App\Http\Resources\User\Catalog\ReviewResource;
use App\Http\Resources\User\UserResource;
use App\ImageTrait;
use App\Services\FilterService;
use App\Services\Order\BonusService;
use App\Services\SIzeService;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $bonus_service = new BonusService();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'discount' => $this->old_price != null ? $bonus_service->calculateDiscount($this->old_price, $this->price) : null,
            'bonus' => $request->user() ? $bonus_service->getBonusByProduct($request->user(), $this->price) : 0,
            'images' => $this->images != null ? ImageTrait::decodeImageArray($this->images) : [],
            'video' => $this->video,
            'stock' => $this->stock,
            'category' => $this->category,
            'favorites' => $request->user() ? $this->userCheckFavorite($request->user()) : false,
            'reviews' => ReviewResource::collection($this->reviews),
            'rating' => $this->rating(),
            'filter' => $this->characteristics != null && $this->characteristics != 'null' ? FilterService::showAsFilters($this->characteristics) : [],
            'old_price' => $this->old_price,
            'sizes' => $this->sizes != null ? SIzeService::showAsSizesAsAvailable($this->sizes) : [],
        ];
    }
}
