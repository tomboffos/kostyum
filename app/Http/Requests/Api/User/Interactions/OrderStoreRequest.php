<?php

namespace App\Http\Requests\Api\User\Interactions;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'spent_bonuses' => 'required|numeric',
            'basket' => 'required',
            'delivery_type_id' => 'required|exists:delivery_types,id',
            'payment_type_id' => 'required|exists:payment_types,id',
            'address_id' => 'nullable|exists:addresses,id',
            'sale_point_id' => 'nullable|exists:sale_points,id'


        ];
    }
}
