<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Catalog\FilterResource;
use App\Models\Category;
use App\Models\Product;
use App\Services\FilterService;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    //
    /**
     * @var FilterService
     */
    private $service;

    public function __construct()
    {
        $this->service = new FilterService();
    }

    public function index(Request $request)
    {
        return $this->service->getFiltersFromCategory($request->category);
    }

    public function show(Category $filter)
    {
        return FilterResource::collection($filter->filters);
    }


}
