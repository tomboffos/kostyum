<?php

namespace App\Http\Controllers\Api\User\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\Catalog\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    //
    public function favoriteInteraction(Product $product, Request $request)
    {
        $request->user()->favorites->contains($product) ?
            $request->user()->favorites()->detach($product) :
            $request->user()->favorites()->attach($product);

        return ProductResource::collection($request->user()->favorites);
    }

    public function index(Request $request)
    {
        return ProductResource::collection($request->user()->favorites);
    }

}
