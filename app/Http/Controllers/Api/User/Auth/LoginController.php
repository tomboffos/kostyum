<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\LoginRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\Interactions\PushService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new PushService();
    }

    public function login(LoginRequest $request)
    {

        $user = User::where('phone', $request->validated()['phone'])->first();

        if ($user && Hash::check($request->validated()['password'], $user['password'])) {
            $this->service->insertToken($user, $request->token);
            return response([
                'user' => new UserResource($user),
                'message' => 'Вы зашли успешно',
                'token' => $user->createToken(env('APP_NAME'))
            ], 200);
        }

        return response([
            'message' => 'Не правильный логин или пароль'
        ], 400);

    }
}
