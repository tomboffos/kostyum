import axios from "axios";

let characteristics = JSON.parse(document.getElementById('characteristics').value)
let categoryId = document.getElementById('category').value
console.log(characteristics)
axios.get(`/api/filters/${categoryId}`).then((response) => {
    response.data.data.forEach((filter, index) => {
        let newDiv = document.createElement('div')
        newDiv.setAttribute('class', 'form-group col-sm-12')
        newDiv.innerHTML = `<label>${filter.name}</label>
                                <input type="hidden" name="characteristics[${index}][filter]" value="${filter.id}" />
                                <select name="characteristics[${index}][value]" class="form-control">
                                    ${filter.values.map((value) => `<option value="${value.value}"  ${characteristics != null ? characteristics.filter((char) => char.value.replaceAll(' ','') === value.value.replaceAll(' ','') && filter.id == char.filter  ).length != 0 ? 'selected' : '' : ''} >${value.value}</option>`)}
                                </select>`
        document.querySelectorAll('.card-body.row')[0].append(newDiv)
    })
})

