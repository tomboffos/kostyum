<?php

namespace App\Http\Resources\User\Info;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'street' => $this->street,
            'flat_number' => $this->flat_number,
            'entrance' => $this->entrance,
            'city' => $this->city
        ];
    }
}
