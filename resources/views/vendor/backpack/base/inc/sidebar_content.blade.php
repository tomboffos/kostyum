<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@if(backpack_user()->role_id == 4  || backpack_user()->role_id == 5)
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i
                class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-list'></i>
            Категории</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i
                class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('filter') }}'><i class='nav-icon la la-filter'></i>
            Фильтры</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('flag') }}'><i class='nav-icon la la-flag'></i>
            Флажки</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('advertisement') }}'><i
                class='nav-icon la la-ad'></i> Реклама</a></li>
    @if(backpack_user()->role_id == 5)
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class='nav-icon la la-user'></i>
                Пользователи</a></li>
    @endif
    <li class='nav-item'><a class='nav-link' href='/users/import'><i class='nav-icon la la-file-import'></i> Импорт</a>
    </li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('city') }}'><i class='nav-icon la la-city'></i>
            Города</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sale-point') }}'><i
                class='nav-icon la la-store'></i> Точки продаж</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('discount') }}'><i
                class='nav-icon la la-percent'></i> Акции</a></li>


    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user-notification') }}'><i
                class='nav-icon la la-circle-notch'></i> Уведомление</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i
                class='nav-icon la la-product-hunt'></i> Продукты</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-type') }}'><i
                class='nav-icon la la-dollar'></i> Типы оплаты</a></li>


@endif

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i class='nav-icon la la-money'></i> Заказы</a>
</li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('size') }}'><i class='nav-icon la la-question'></i>
        Размер</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i
            class='nav-icon la fa-facebook-messenger'></i> Отзывы</a></li>
