<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderPaymentOfflineTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withHeaders([
            'Authorization'  => 'Bearer 3|Lry1x0RYECET3nqe4cMy7d2KeboxKrzfMzOz80dz'
        ])->postJson('/api/orders',[
            'basket' => json_encode([
                [
                    'product_id' => Product::inRandomOrder()->first()->id,
                    'quantity' => 2
                ],
                [
                    'product_id' => Product::inRandomOrder()->first()->id,
                    'quantity' => 1
                ]
            ]),
            'delivery_type_id' => 1,
            'payment_type_id' => 3,
            'spent_bonuses' => 0,
            'address_id' => 1
        ]);

        $response->dump();
        $response->assertStatus(200);
    }
}
