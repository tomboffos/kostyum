<?php

namespace App\Observers;

use App\Models\BonusHistory;

class BonusHistoryObserver
{
    /**
     * Handle the BonusHistory "created" event.
     *
     * @param  \App\Models\BonusHistory  $bonusHistory
     * @return void
     */
    public function created(BonusHistory $bonusHistory)
    {
        //
        $bonusHistory->order->update([
            'bonus' => $bonusHistory->bonus_amount
        ]);
    }

    /**
     * Handle the BonusHistory "updated" event.
     *
     * @param  \App\Models\BonusHistory  $bonusHistory
     * @return void
     */
    public function updated(BonusHistory $bonusHistory)
    {
        //
    }

    /**
     * Handle the BonusHistory "deleted" event.
     *
     * @param  \App\Models\BonusHistory  $bonusHistory
     * @return void
     */
    public function deleted(BonusHistory $bonusHistory)
    {
        //
    }

    /**
     * Handle the BonusHistory "restored" event.
     *
     * @param  \App\Models\BonusHistory  $bonusHistory
     * @return void
     */
    public function restored(BonusHistory $bonusHistory)
    {
        //
    }

    /**
     * Handle the BonusHistory "force deleted" event.
     *
     * @param  \App\Models\BonusHistory  $bonusHistory
     * @return void
     */
    public function forceDeleted(BonusHistory $bonusHistory)
    {
        //
    }
}
