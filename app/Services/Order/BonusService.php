<?php


namespace App\Services\Order;


use App\Models\BonusHistory;
use App\Models\Discount;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class BonusService
{
    const CONDITIONS = [
        'quantity',
        'amount',
        'sum_amount'
    ];

    public function calculateDiscount($oldPrice, $price)
    {
        return 100 - ($price * 100) / $oldPrice;
    }

    public function getBonusByProduct(User $user, $productPrice)
    {
        if (Discount::where('amount', '<=', $user->order_sum - $user->spentBonusSum() + $productPrice)->where('condition', self::CONDITIONS[2])->exists())
            return Discount::where('amount', '<=', $user->order_sum - $user->spentBonusSum() + $productPrice)
                ->where('condition', self::CONDITIONS[2])
                ->orderBy('amount', 'desc')->first()->bonus_type == 'percent' ? Discount::where('amount', '<=', $user->order_sum - $user->spentBonusSum() + $productPrice)
                    ->where('condition', self::CONDITIONS[2])
                    ->orderBy('amount', 'desc')->first()->value * 0.01 * $productPrice : Discount::where('amount', '<=', $user->order_sum - $user->spentBonusSum() + $productPrice)
                ->where('condition', self::CONDITIONS[2])
                ->orderBy('amount', 'desc')->first()->value;
        else
            return 0;
    }

    public function checkSumBonus(Order $order)
    {
        if (Discount::where('amount', '<=', $order->user->order_sum - $order->user->spentBonusSum())->where('condition', self::CONDITIONS[2])->exists())
            $this->giveBonus(Discount::where('amount', '<=', $order->user->order_sum - $order->user->spentBonusSum())
                ->where('condition', self::CONDITIONS[2])
                ->orderBy('amount', 'desc')->first(), $order);
    }

    public function checkBonuses(Order $order)
    {
        $this->checkPriceBonus($order);
        $this->checkSumBonus($order);
        foreach (Discount::where('amount', $order->user->order_sum)->where('condition', self::CONDITIONS[0])->where(function ($query) use ($order) {
            $query->where('payment_type_id', $order->payment_type_id)
                ->orWhereNull('payment_type_id', $order->payment_type_id);
        })->get() as $discount) $this->giveBonus($discount, $order);

    }

    public function checkPriceBonus($order)
    {
        if (Discount::where('amount', '<=', $order->price)->where('condition', self::CONDITIONS[1])->exists())
            $this->giveBonus(Discount::where('amount', '<=', $order->price)
                ->where('condition', self::CONDITIONS[1])
                ->orderBy('amount', 'desc')->first(), $order);
    }

    public function giveBonus(Discount $discount, Order $order)
    {
        $fact_price = $order->price - $order->spent_bonuses;
        $order->user->update([
            'balance' => $discount->bonus_type == 'percent'
                ? $order->user->balance + intval($fact_price * ($discount->value * 0.01))
                : $order->user->balance - $order->spent_bonuses + $discount->value
        ]);


        BonusHistory::create([
            'user_id' => $order->user->id,
            'discount_id' => $discount->id,
            'bonus_amount' => $discount->bonus_type == 'percent'
                ? intval($fact_price * ($discount->value * 0.01))
                : $discount->value,
            'order_id' => $order->id
        ]);

    }


    public function refund(Order $order)
    {
        $order->user->update([
            'balance' => $order->user->balance + $order->spent_bonuses
        ]);



        foreach ($order->receivedBonuses as $bonusHistory)
            $order->user->update([
                'balance' => $order->user->balance - $bonusHistory->bonus_amount,
                'order_sum' => $order->user->order_sum - $order->price
            ]);

        $order->updateQuietly([
            'spent_bonuses' => 0,
            'bonus' => 0
        ]);

    }
}
