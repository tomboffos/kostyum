<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class UsersImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (!User::where('phone', str_replace(' ','',$row[1]))->exists())
            return new User([
                'name' => $row[0],
                'phone' => $row[1],
                'balance' => intval($row[3]),
                'password' => '12345678',
                'role_id' => 1,
                'order_sum' => intval($row[2])
            ]);
        else
            User::where('phone', str_replace(' ','',$row[1]))->first()->update([
                'name' => $row[0],
                'phone' => $row[1],
                'balance' => intval($row[3]),
                'order_sum' => intval($row[2])
            ]);
    }


}
