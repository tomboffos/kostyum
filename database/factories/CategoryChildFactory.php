<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryChildFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word,
            'order' => $this->faker->randomDigitNotNull,
            'image' => $this->faker->imageUrl,
            'category_id' => Category::whereNull('category_id')->inRandomOrder()->first()->id
        ];
    }
}
