<?php

namespace App\Observers;

use App\Models\Order;
use App\Services\Interactions\PushService;
use App\Services\Order\BonusService;
use Illuminate\Support\Facades\Log;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //
        $bonus = new BonusService();
        if ($order->spent_bonuses != 0)
            $order->user->update([
                'balance' => $order->user->balance - $order->spent_bonuses
            ]);

        if ($order->payment_status_id == 2) {
            $order->user->update([
                'order_sum' => $order->user->order_sum + $order->price
            ]);

            $bonus->checkBonuses($order);
            PushService::send($order->user->devices(), 'Поздравляем с покупкой!', 'Спасибо за покупку заказа номер ' . $order->id);
        }


    }

    /**
     * Handle the Order "updated" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        $bonus = new BonusService();
        //
        if (($order->getOriginal('payment_status_id') != $order->payment_status_id) && $order->payment_status->id == 2) {
            $order->user->update([
                'order_sum' => $order->user->order_sum + $order->price
            ]);

            $bonus->checkBonuses($order);
        }

        if ($order->getOriginal('order_status_id') != $order->order_status_id) {
            if ($order->order_status_id == 5) {
                $bonus->refund($order);
            }
            PushService::send($order->user->devices(),
                'Статус вашего заказа ' . $order->id,
                'Изменен статус заказа: ' . $order->order_status->name);

            if ($order->order_status_id == 6) {
                $order->user->update([
                    'balance' => $order->user->balance + $order->spent_bonuses
                ]);

                $order->updateQuietly([
                    'spent_bonuses' => 0
                ]);
            }
        }

        if ($order->getOriginal('courier_id') != $order->courier_id)
            PushService::send($order->courier->devices(), 'Новый заказ ', 'Вам задан новый заказ ' . $order->id);
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
