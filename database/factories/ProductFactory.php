<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word,
            'category_id' => Category::inRandomOrder()->first()->id,
            'price' => mt_rand(2000, 50000),
            'discount' => mt_rand(0, 100),
            'bonus' => mt_rand(0, 2000),
            'images' => json_encode([
                $this->faker->imageUrl,
                $this->faker->imageUrl,
                $this->faker->imageUrl
            ]),
            'video' => 'https://www.youtube.com/watch?v=n5a_DMlNIBc',
            'vendor' => Str::random(20),
            'stock' => true

        ];
    }
}
