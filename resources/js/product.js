import axios from "axios";


document.getElementById('category').addEventListener('change', (e) => {
    console.log(e.target.value)
    axios.get(`/api/filters/${e.target.value}`).then((response) => {
        response.data.data.forEach((filter, index) => {
            let newDiv = document.createElement('div')
            newDiv.setAttribute('class', 'form-group col-sm-12')
            newDiv.innerHTML = `<label>${filter.name}</label>
                                <input type="hidden" name="characteristics[${index}][filter]" value="${filter.id}" />
                                <select name="characteristics[${index}][value]" class="form-control">
                                    ${filter.values.map((value) => `<option value="${value.value}">${value.value}</option>`)}
                                </select>`
            document.querySelectorAll('.card-body.row')[0].append(newDiv)
        })
    })
})
