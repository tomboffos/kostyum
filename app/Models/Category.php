<?php

namespace App\Models;

use Database\Factories\CategoryChildFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'category_id',
        'order',
        'image',
    ];

    public function childCount()
    {
        return $this->hasMany(Category::class)->count();
    }

    protected static function newFactory()
    {
        return CategoryChildFactory::new();
    }

    public function children()
    {
        return $this->hasMany(Category::class)->orderBy('order', 'asc');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class,'filter_category');
    }

}
