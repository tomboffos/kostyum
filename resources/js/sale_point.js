function initMap(currentPosition){
    console.log(currentPosition)
    const uluru = { lat: currentPosition.coords.latitude, lng: currentPosition.coords.longitude };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: uluru,
    });


    map.addListener('click',(e)=>{
        addPolygons(e.latLng,map)

    })
}
let marker = {}
let newMarker = new google.maps.Marker({
    position: {},
    map,
})

function addPolygons(latLng,map){
    marker = latLng
    newMarker.setMap(null)
    newMarker = new google.maps.Marker({
        position: marker,
        map,
    })
    setField(latLng)
}

function setField(latLng) {
    let areaField = document.querySelectorAll('input[name="location"]')
    areaField[0].value = JSON.stringify(latLng)
}

if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(initMap);
}
