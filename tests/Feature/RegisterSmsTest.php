<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterSmsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/auth/forget/56', [
            'code' => 6317,
            'password' => '1107200Asyl'
        ]);

        $response->dump();
        $response->assertStatus(200);
    }
}
