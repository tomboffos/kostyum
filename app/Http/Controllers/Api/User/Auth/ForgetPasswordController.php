<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\User\Auth\SetNewPasswordRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Services\Interactions\SmsService;
use Illuminate\Http\Request;

class ForgetPasswordController extends Controller
{
    //
    private $sms;

    public function __construct()
    {
        $this->sms = new SmsService();
    }

    public function forgetRequest(ForgetPasswordRequest $request)
    {
        return response([
            'sms' => $this->sms->createOrUpdateSms(User::where('phone', $request->phone)->first()),
            'user' => new UserResource(User::where('phone', $request->phone)->first())
        ]);
    }

    public function setNewPassword(User $user, SetNewPasswordRequest $request)
    {
        if ($this->sms->checkSms($request->code, $user)) {
            $user->update([
                'password' => $request->password,
            ]);

            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(env('APP_NAME'))->plainTextToken,

            ], 200);
        } else
            return response([
                'message' => 'Не верный код'
            ], 400);
    }
}
