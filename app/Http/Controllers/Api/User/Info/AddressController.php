<?php

namespace App\Http\Controllers\Api\User\Info;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Info\AddressStoreRequest;
use App\Http\Resources\User\Info\AddressResource;
use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    //
    public function index(Request $request)
    {
        return AddressResource::collection($request->user()->addresses);
    }

    public function store(AddressStoreRequest $request)
    {
        $data = $request->validated();

        $data['user_id'] = $request->user()->id;


        return new AddressResource(Address::create($data));
    }

    public function destroy(Address $address)
    {
        return $address->delete();
    }
}
