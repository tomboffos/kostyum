<?php

use App\Http\Controllers\Api\Courier\CourierOrderController;
use App\Http\Controllers\Api\User\Auth\ForgetPasswordController;
use App\Http\Controllers\Api\User\Auth\LoginController;
use App\Http\Controllers\Api\User\Auth\RegisterController;
use App\Http\Controllers\Api\User\Catalog\AdvertisementController;
use App\Http\Controllers\Api\User\Catalog\CategoryController;
use App\Http\Controllers\Api\User\Catalog\DiscountController;
use App\Http\Controllers\Api\User\Catalog\FavoriteController;
use App\Http\Controllers\Api\User\Catalog\FilterController;
use App\Http\Controllers\Api\User\Catalog\FlagController;
use App\Http\Controllers\Api\User\Catalog\ProductController;
use App\Http\Controllers\Api\User\Catalog\ReviewController;
use App\Http\Controllers\Api\User\Info\AddressController;
use App\Http\Controllers\Api\User\Info\UserController;
use App\Http\Controllers\Api\User\Interactions\CityController;
use App\Http\Controllers\Api\User\Interactions\DeliverTypeController;
use App\Http\Controllers\Api\User\Interactions\OrderController;
use App\Http\Controllers\Api\User\Interactions\PaymentController;
use App\Http\Controllers\Api\User\SalePointController;
use App\Http\Controllers\Api\Vendor\Interactions\UserVendorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [LoginController::class, 'login']);
    Route::group(['prefix' => 'register'], function () {
        Route::post('/', [RegisterController::class, 'register']);
        Route::post('/{user}', [RegisterController::class, 'check']);
    });
    Route::group(['prefix' => 'forget'], function () {
        Route::post('/', [ForgetPasswordController::class, 'forgetRequest']);
        Route::post('/{user}', [ForgetPasswordController::class, 'setNewPassword']);
    });
});


Route::resources([
    '/categories' => CategoryController::class,
    '/products' => ProductController::class,
    '/advertisements' => AdvertisementController::class,
    '/delivery-types' => DeliverTypeController::class,
    '/payment-types' => PaymentController::class,
    '/filters' => FilterController::class,
    '/flags' => FlagController::class,
    '/sale-points' => SalePointController::class,
    '/discounts' => DiscountController::class,
    '/cities' => CityController::class
]);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('/favorites/{product}', [FavoriteController::class, 'favoriteInteraction']);
    Route::get('/favorites ', [FavoriteController::class, 'index']);
    Route::get('/auth/discount', [DiscountController::class, 'getDiscount']);
    Route::resources([
        '/user' => UserController::class,
        '/reviews' => ReviewController::class,
        '/addresses' => AddressController::class,
        '/orders' => OrderController::class,
        '/auth/products' => ProductController::class,
    ]);

    Route::group(['prefix' => 'courier'], function () {
        Route::resource('/orders', CourierOrderController::class);
    });

    Route::group(['prefix' => 'vendor'], function () {
        Route::resources([
            '/orders' => \App\Http\Controllers\Api\Vendor\Interactions\OrderController::class,
            '/users' => UserVendorController::class
        ]);
    });
});

