<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'balance',
        'image',
        'role_id',
        'order_sum',
        'receive_notifications'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function courierOrders()
    {
        return $this->hasMany(Order::class, 'courier_id', 'id');
    }

    public function vendorOrders(Request $request)
    {
        return $this->hasMany(Order::class, 'vendor_id', 'id')->where(function ($query) use ($request) {
            if ($request->has('search')) $query->where('id', 'LIKE', '%' . $request->search . '%');
        })->get();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function favorites()
    {
        return $this->belongsToMany(Product::class, 'favorites');
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = $value == null ? null : $value->store('/avatars');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('id', 'desc');
    }

    public function ordersCount()
    {
        return $this->hasMany(Order::class)->count();
    }

    public function tokenExists($token)
    {
        return $this->hasMany(UserDevice::class)->where('token', $token)->exists();
    }


    public function devices()
    {
        return $this->hasMany(UserDevice::class)->pluck('token');
    }

    public function payedOrdersCount()
    {
        return $this->hasMany(Order::class)->where('payment_status_id', 2)->count();
    }

    public function orderSum()
    {
        return $this->hasMany(Order::class)->where('payment_status_id', 2)
            ->where('order_status_id', '!=', 5)->sum('price');
    }

    public function spentBonusSum()
    {
        return $this->hasMany(Order::class)
            ->where('order_status_id', '!=', 5)
            ->where('payment_status_id', 2)->sum('spent_bonuses');
    }


}
