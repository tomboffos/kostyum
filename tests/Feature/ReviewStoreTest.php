<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReviewStoreTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer 1|CQ0bbVVGo2ij7tNapfQJmbXr8809odSB3T8koSXb',
        ])->postJson('/api/reviews',[
            'advantages' => 'OK',
            'comments' => 'Awesome',
            'rating' => 5,
            'product_id' => Product::inRandomOrder()->first()->id,

        ]);

        $response->assertStatus(201);
    }
}
