<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FilterTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('filter_types')->insert([
            'name' => 'Одиночный выбор',
            'id' => 1
        ]);

        DB::table('filter_types')->insert([
            'name' => 'Множественный выбор',
            'id' => 2
        ]);
    }
}
