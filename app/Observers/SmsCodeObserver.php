<?php

namespace App\Observers;

use App\Models\SmsCode;
use App\Services\Interactions\SmsService;

class SmsCodeObserver
{
    /**
     * Handle the SmsCode "created" event.
     *
     * @param \App\Models\SmsCode $smsCode
     * @return void
     */
    public function created(SmsCode $smsCode)
    {
        //
        SmsService::send($smsCode->user->phone, "https://kostyum.kz Вам отправлен смc подтверждение: $smsCode->code");
    }

    /**
     * Handle the SmsCode "updated" event.
     *
     * @param \App\Models\SmsCode $smsCode
     * @return void
     */
    public function updated(SmsCode $smsCode)
    {
        //
        if ($smsCode->getOriginal('code') != $smsCode->code)
            SmsService::send($smsCode->user->phone, "https://kostyum.kz Вам отправлен смc подтверждение: $smsCode->code");


    }

    /**
     * Handle the SmsCode "deleted" event.
     *
     * @param \App\Models\SmsCode $smsCode
     * @return void
     */
    public function deleted(SmsCode $smsCode)
    {
        //
    }

    /**
     * Handle the SmsCode "restored" event.
     *
     * @param \App\Models\SmsCode $smsCode
     * @return void
     */
    public function restored(SmsCode $smsCode)
    {
        //
    }

    /**
     * Handle the SmsCode "force deleted" event.
     *
     * @param \App\Models\SmsCode $smsCode
     * @return void
     */
    public function forceDeleted(SmsCode $smsCode)
    {
        //
    }
}
