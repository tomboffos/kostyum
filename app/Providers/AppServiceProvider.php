<?php

namespace App\Providers;

use App\Models\BonusHistory;
use App\Models\Order;
use App\Models\SmsCode;
use App\Models\UserNotification;
use App\Observers\BonusHistoryObserver;
use App\Observers\OrderObserver;
use App\Observers\SmsCodeObserver;
use App\Observers\UserNotificationObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Order::observe(OrderObserver::class);
        UserNotification::observe(UserNotificationObserver::class);
        SmsCode::observe(SmsCodeObserver::class);
        BonusHistory::observe(BonusHistoryObserver::class);
    }
}
