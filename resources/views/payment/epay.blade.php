<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<input type="hidden" name="order" value="{{json_encode($order)}}" id="order">
<input type="hidden" name="invoice_id" value="{{$invoice_id}}" id="invoice_id">
<script src="https://epay.homebank.kz/payform/payment-api.js"></script>
<script>
    let order = JSON.parse(document.getElementById('order').value)
    let invoiceId = document.getElementById('invoice_id').value
    let createPaymentObject = function (auth) {
        console.log(order)
        var paymentObject = {
            invoiceId: invoiceId,
            backLink: "https://example.kz/success.html",
            failureBackLink: "https://example.kz/failure.html",
            postLink: `https://kostyum.it-lead.net/epay/order/${order.id}`,
            failurePostLink: "https://example.kz/order/1123/fail",
            language: "RU",
            description: "Оплата в интернет магазине",
            accountId: order.user.id,
            terminal: "9da10824-6c84-4949-bd97-70322464cfd1",
            amount: order.spent_bonuses != 0 ? order.price - order.spent_bonuses : order.price,
            currency: "KZT",
            phone: order.user.name,
            email: "example@example.com",
            cardSave: true
        };
        paymentObject.auth = JSON.parse(auth);
        console.log(paymentObject)
        return paymentObject;
    };
    createPaymentObject(order.auth)
    halyk.pay(createPaymentObject(order.auth));
</script>

</body>
</html>
