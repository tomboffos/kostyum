<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Product;
use App\Services\FilterService;
use Illuminate\Console\OutputStyle;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithCustomCsvSettings, WithHeadingRow, WithChunkReading, ShouldQueue, WithBatchInserts
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
//        print_r($row);

        $filter_service = new FilterService();
        $category = null;
        if ($row['tip_tovarov'] != null) {
            $category = Category::where('name', $row['tip_tovarov'])->first();
            if ($category == null)
                $category = Category::create([
                    'name' => $row['tip_tovarov']
                ]);
        }
//
        $product = Product::where('origin_product_id', $row['id_tovara'])->first();
        if ($product == null)
            return new Product([
                //
                'name' => $row['naimenovanie'] != '' ? $row['naimenovanie'] : '',
                'vendor' => $row['kod_artikula'],
                'price' => $row['cena'] != null ? $row['cena'] : 0,
                'discount' => 0,
                'bonus' => 0,
                'category_id' => $category != null ? $category->id : Category::inRandomOrder()->first()->id,
                'description' => $row['opisanie'],
                'origin_product_id' => $row['id_tovara'],
                'video' => $row['adres_video_na_youtube_ili_vimeo'],
                'images' => $row['izobrazeniya_tovarov'] != null ? json_encode([
                    $row['izobrazeniya_tovarov']
                ]) : null
            ]);
        else
            $product->update([
                'name' => $row['naimenovanie'] != null ? $row['naimenovanie'] : '',
                'vendor' => $row['kod_artikula'],
                'price' => $row['cena'] != null ? $row['cena'] : 0,
                'discount' => 0,
                'bonus' => 0,
                'category_id' => $category != null ? $category->id : Category::inRandomOrder()->first()->id,
                'description' => $row['opisanie'],
                'origin_product_id' => $row['id_tovara'],
                'characteristics' => $filter_service->constructCharacteristics($row),
                'video' => $row['adres_video_na_youtube_ili_vimeo'],
                'images' => $row['izobrazeniya_tovarov'] != null ? json_encode([
                    $row['izobrazeniya_tovarov']
                ]) : null

            ]);
    }

//    public function headingRow(): int
//    {
//        return 2;
//    }

    public function getCsvSettings(): array
    {
        return [
            'input_encoding' => 'windows-1251',
            'delimiter' => "\t"
        ];
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public function batchSize(): int
    {
        return 500;
    }
}
