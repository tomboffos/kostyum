<?php


namespace App\Services;


use App\Http\Requests\Api\User\Interactions\OrderStoreRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderService
{
    public function mergeOrderArrays(OrderStoreRequest $request)
    {

        $data = $request->validated();



        $initialData = [
            'user_id' => $request->user()->id,
            'payment_status_id' => 1,
            'order_status_id' => 1
        ];


        return array_merge($data, $initialData, $this->getPriceAndBonuses($data['basket']));
    }


    public function mergeOrderArraysForVendors(\App\Http\Requests\Api\Vendor\Interactions\OrderStoreRequest $request)
    {

        $data = $request->validated();



        $initialData = [
            'payment_status_id' => 2,
            'order_status_id' => 4,
            'quantity' => 0,
            'bonus' => 0,
            'vendor_id' => $request->user()->id,
        ];


        return array_merge($data, $initialData);
    }

    public function getPriceAndBonuses($basket)
    {
        $price = 0;
        $bonus = 0;
        $quantity = 0;
        foreach (json_decode($basket,true) as $item) {
            $product = Product::find($item['product_id']);

            $price += $product->price * $item['quantity'];
            $bonus += $product->bonus * $item['quantity'];
            $quantity += $item['quantity'];
        }

        return [
            'price' => $price,
            'quantity' => $quantity,
            'bonus' => $bonus
        ];
    }

    public function iterateBasket($basket, Order $order)
    {
        foreach (json_decode($basket,true) as $item) {
            $item['order_id'] = $order->id;
            OrderDetail::create($item);
        }
    }
}
